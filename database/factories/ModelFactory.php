<?php

$factory->define(Tcc\Models\Empresa::class, function (Faker\Generator $faker) {

    return [
        'razao_social' => $faker->name,
        'fantasia' => $faker->company,
        'cnpj' => $faker->numerify('##############'),
        'email' => $faker->unique()->safeEmail,
        'telefone' => $faker->phoneNumber,
        'ramal' => $faker->numerify('###'),
        'celular' => $faker->tollFreePhoneNumber,
        'cep' => $faker->postcode,
        'endereco' => $faker->streetName,
        'numero' => $faker->numerify('####'),
        'complemento' => $faker->secondaryAddress,
        'bairro' => $faker->citySuffix,
        'nome_do_responsavel' => $faker->name,
        'email_do_responsavel' => $faker->email
    ];
});


$factory->define(Tcc\Models\User::class, function (Faker\Generator $faker) {

    return [
        'login' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt(1),
        'ativo' => 1,
        'remember_token' => str_random(10),
        'empresa_id' => \Tcc\Models\Empresa::all()->random()->id
    ];
});

$factory->define(Tcc\Models\Cliente::class, function (Faker\Generator $faker) {

    return [
        'nome' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'telefone' => $faker->phoneNumber,
        'empresa_id' => \Tcc\Models\Empresa::all()->random()->id,
        'cep' => $faker->postcode,
        'endereco' => $faker->streetName,
        'numero' => $faker->numerify('####'),
        'complemento' => $faker->secondaryAddress,
        'bairro' => $faker->citySuffix
    ];
});

$factory->define(Tcc\Models\Categoria::class, function (Faker\Generator $faker) {

    return [
        'categoria' => $faker->word,
        'empresa_id' => \Tcc\Models\Empresa::all()->random()->id,
        'ativo' => $faker->randomElement(['0','1']),
    ];
});

$factory->define(Tcc\Models\Produto::class, function (Faker\Generator $faker) {
    $empresa = \Tcc\Models\Empresa::all()->random()->id;

    return [
        'produto' => $faker->word,
        'descricao' => $faker->sentence(10),
        'categoria_id' => \Tcc\Models\Categoria::where('empresa_id',$empresa)->get()->random()->id,
        'empresa_id' => $empresa,
        'preco' => $faker->randomFloat(2,1,999),
        'ativo' => $faker->randomElement(['0','1']),
    ];
});