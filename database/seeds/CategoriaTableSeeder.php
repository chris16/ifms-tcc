<?php

use Illuminate\Database\Seeder;
use Tcc\Models\Categoria;

class CategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Categoria::class,50)->create();
    }
}
