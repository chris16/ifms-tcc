<?php

use Illuminate\Database\Seeder;
use Tcc\Models\Empresa;
use Tcc\Models\User;

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //fixo
        factory(User::class)->create([
            'login' => 'admin',
            'email' => 'admin@ifms.edu.br',
            'password' => bcrypt('1'),
            'remember_token' => str_random(10),
            'ativo' => 1,
            'empresa_id' => Empresa::all()->random()->id
        ]);

        factory(User::class, 5)->create();
    }
}
