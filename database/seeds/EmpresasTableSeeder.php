<?php

use Illuminate\Database\Seeder;
use Tcc\Models\Cargo;
use Tcc\Models\Cidade;
use Tcc\Models\Empresa;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Empresa::class,10)->create();
    }
}
