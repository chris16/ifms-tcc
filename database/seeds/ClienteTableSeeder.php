<?php

use Illuminate\Database\Seeder;
use Tcc\Models\Cliente;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Cliente::class,50)->create();
    }
}
