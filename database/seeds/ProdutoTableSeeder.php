<?php

use Illuminate\Database\Seeder;
use Tcc\Models\Produto;

class ProdutoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Produto::class,100)->create();
    }
}
