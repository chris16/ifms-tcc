<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nome');
            $table->string('email',100);
			$table->string('telefone',30);
			$table->string('cep',10);
			$table->string('endereco',100);
			$table->smallInteger('numero');
			$table->string('complemento',100)->nullable();
			$table->string('bairro',100);

			$table->integer('empresa_id')->unsigned();
			$table->foreign('empresa_id')->references('id')->on('empresas');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clientes');
	}

}
