<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empresas', function(Blueprint $table) {
			$table->increments('id');
			$table->string('razao_social',150);
			$table->string('logomarca',60)->nullable();
			$table->string('fantasia',150);
			$table->string('cnpj',18);
			$table->string('email',100);
			$table->string('telefone',30);
			$table->smallInteger('ramal')->nullable()->default(0);
			$table->string('celular',16);
			$table->string('cep',10);
			$table->string('endereco',100);
			$table->smallInteger('numero');
			$table->string('complemento',100)->nullable();
			$table->string('bairro',100);
			$table->string('nome_do_responsavel',60);
			$table->string('email_do_responsavel',150);
			$table->enum('ativo',[0,1])->default(1);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empresas');
	}

}
