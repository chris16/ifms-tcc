<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::post('/registrar', 'UsersController@register')->name('user.register');

Route::group(['prefix' => 'sistema', 'as' => 'sistema.','middleware' => 'auth'], function () {

    //EMPRESA
    Route::get('/minha-empresa', ['as' => 'empresas.edit','uses' =>'EmpresasController@edit']);
    Route::put('/editar-cadastro', ['as' => 'empresas.update','uses' =>'EmpresasController@update']);

    //CATEGORIAS
    Route::get('/categorias','CategoriasController@show')->name('categoria.show');
    Route::post('/nova-categoria', 'CategoriasController@store')->name('categoria.store');
    Route::get('/mudar-status-categoria/{id}/{status}','CategoriasController@changeStatus')->name('categoria.change');
    Route::get('/remover-categoria/{id}/','CategoriasController@softDelete')->name('categoria.delete');

    //CLIENTES
    Route::get('/clientes', ['as' => 'clientes.showAll','uses' =>'ClientesController@showAll']);
    Route::get('/novo-cliente', ['as' => 'clientes.create','uses' =>'ClientesController@create']);
    Route::post('/cadastrar-cliente', ['as' => 'clientes.store','uses' =>'ClientesController@store']);
    Route::get('/editar-cliente/{id}', ['as' => 'clientes.edit','uses' =>'ClientesController@edit']);
    Route::put('/editar-cadastro-cliente/{id}', ['as' => 'clientes.update','uses' =>'ClientesController@update']);

    //FORMAS DE PAGAMENTO
    Route::get('/formas-de-pagamento','FormasPagamentoController@show')->name('formas.show');
    Route::post('/nova-formas-de-pagamento', 'FormasPagamentoController@store')->name('formas.store');
    Route::get('/mudar-status-formas-de-pagamento/{id}/{status}','FormasPagamentoController@changeStatus')->name('formas.change');

    //PEDIDO
    Route::get('/novo-pedido','PedidosController@create')->name('pedido.create');
    Route::post('/adicionar-item-pedido', 'PedidosController@addToCart')->name('pedido.addItem');
    Route::post('/salvar-pedido', 'PedidosController@store')->name('pedido.store');
    Route::post('/remover-item-pedido', 'PedidosController@removeItemPedido')->name('carrinho.remove');
    Route::get('/pedidos', 'PedidosController@showAll')->name('pedido.showAll');

    //PRODUTOS
    Route::get('/produtos','ProdutosController@showAll')->name('produto.showAll');
    Route::get('/novo-produto', 'ProdutosController@create')->name('produto.create');
    Route::post('/cadastrar-produto', 'ProdutosController@store')->name('produto.store');
    Route::get('/editar-produto/{id}','ProdutosController@edit')->name('produto.edit');
    Route::put('/editar-cadastro-produto/{id}','ProdutosController@update')->name('produto.update');
    Route::post('/ajax-busca-produto-por-categoria','ProdutosController@findProdutosPorCategoriaAjax');

});


