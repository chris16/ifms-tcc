<?php

namespace Tcc\Criteria;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FiltroPorEmpresaCriteria
 * @package namespace Tcc\Criteria;
 */
class FiltroPorEmpresaCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $empresaId = auth()->user()->empresa_id;
        $model = $model->where('empresa_id',$empresaId);
        
        return $model;
    }
}
