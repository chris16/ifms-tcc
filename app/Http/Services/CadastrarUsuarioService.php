<?php

namespace Tcc\Http\Services;


use Tcc\Repositories\EmpresaRepository;
use Tcc\Repositories\UserRepository;

class CadastrarUsuarioService
{

    /** @var  UserRepository */
    protected $usuarioRepository;

    /** @var  EmpresaRepository */
    protected $empresaRepository;

    /**
     * CadastrarUsuarioService constructor.
     * @param UserRepository $usuarioRepository
     * @param EmpresaRepository $empresaRepository
     */
    public function __construct(UserRepository $usuarioRepository, EmpresaRepository $empresaRepository)
    {
        $this->usuarioRepository = $usuarioRepository;
        $this->empresaRepository = $empresaRepository;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function save(array $data)
    {
        try {
            $empresa = $this->saveEmpresa($data);
            $this->saveUsuario($data, $empresa->id);

            return true;
        } catch (\Exception $e) {
            flash()->error($e->getMessage());

            return false;
        }
    }

    /**
     * @param array $data
     * @return mixed
     */
    private function saveEmpresa(array $data)
    {
        $empresa = $this->empresaRepository->create($data);

        return $empresa;
    }

    /**
     * @param array $data
     * @param $empresaId
     * @return mixed
     */
    private function saveUsuario(array $data, $empresaId)
    {
        $data['empresa_id'] = $empresaId;
        $data['ativo'] = 1;
        $data['password'] = bcrypt($data['password']);
        $usuario = $this->usuarioRepository->create($data);

        return $usuario;
    }


}