<?php

namespace Tcc\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProdutoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'produto' => [
                'required',
                'min:5',
                Rule::unique('produtos')->ignore($this->id)->where(function ($query) {
                    $query->where('empresa_id', auth()->user()->empresa_id);
                })
            ],
            'categoria_id' => 'required',
            'preco' => 'required',
            'ativo' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'produto.required' => 'Por favor, preencha o campo PRODUTO.',
            'produto.min' => 'Por favor, informe um NOME para o PRODUTO de no mínimo :min caracteres.',
            'produto.unique' => 'Esse PRODUTO já foi cadastrado.',
            'categoria_id.required' => 'Por favor, preencha o campo CATEGORIA.',
            'preco.required' => 'Por favor, preencha o campo PREÇO.',
            'ativo.required' => 'Por favor, preencha o campo ATIVO.',
        ];
    }
}
