<?php

namespace Tcc\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormaPagamentoCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'forma_pagamento' => 'required|min:5'
        ];
    }

    public function messages()
    {
        return [
            'forma_pagamento.required' => 'Por favor, preencha o campo FORMA DE PAGAMENTO.',
            'forma_pagamento.min' => 'Por favor, informe um FORMA DE PAGAMENTO de no mínimo :min caracteres.',
        ];
    }
}
