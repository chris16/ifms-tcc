<?php

namespace Tcc\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PedidoCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cliente_id' => 'required',
            'forma_pagamento_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'cliente_id.required' => 'Por favor, informe o CLIENTE.',
            'forma_pagamento_id.required' => 'Por favor, informe a FORMA DE PAGAMENTO.',
        ];
    }
}
