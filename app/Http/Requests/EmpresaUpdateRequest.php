<?php

namespace Tcc\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresaUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razao_social' => 'required|min:5',
            'fantasia' => 'required',
            'cnpj' => 'required|digits:13|unique:empresas,id,'.$this->get('id'),
            'email' => 'required|email|max:150|unique:empresas,id,'.$this->get('id'),
            'telefone' => 'required',
            'endereco' => 'required',
            'numero' => 'required',
            'cep' => 'required',
            'nome_do_responsavel' => 'required',
            'email_do_responsavel' => 'required|email',
            'ativo' => 'required|boolean'
        ];

    }

    public function messages()
    {
        return [
            'razao_social.required' => 'Por favor, preencha o campo RAZÃO SOCIAL.',
            'razao_social.min' => 'Por favor, informe um RAZÃO SOCIAL de no mínimo :min caracteres.',
            'fantasia.required' => 'Por favor, preencha o campo FANTASIA.',
            'cnpj.required' => 'Por favor, preencha o campo CNPJ.',
            'cnpj.unique' => 'O CNPJ informado para a empresa já está cadastrado no sistema.',
            'email.required' => 'Por favor, preencha o campo E-MAIL.',
            'email.email' => 'Por favor, o formato de e-mail está inválido.',
            'email.unique' => 'O E-MAIL informado já está cadastrado no sistema.',
            'email.max' => 'Por favor, informe um E-MAIL de no máximo :max caracteres.',
            'endereco.required' => 'Por favor, preencha o campo CATEGORIA.',
            'telefone.required' => 'Por favor, preencha o campo DESCRIÇÃO.',
            'numero.required' => 'Por favor, preencha o campo PRAZO DE PRODUÇÃO.',
            'cep.required' => 'Por favor, ipreencha o campo CEP.',
            'bairro.required' => 'Por favor, preencha o campo BAIRRO.',
            'cidade_id.required' => 'Por favor, preencha o campo CIDADE.',
            'nome_do_responsavel.required' => 'Por favor, preencha o campo LARGURA.',
            'email_do_responsavel.required' => 'Por favor, preencha o campo E-MAIL.',
            'email_do_responsavel.email' => 'Por favor, o formato de e-mail está inválido.',
            'ativo.required' => 'Por favor, informe se a EMPRESA estará ativo.',
            'ativo.boolean' => 'Por favor, o campo ATIVO deve ser 0 ou 1.',
            'cnpj.digits' => 'Por favor, o campo CNPJ deve possuir 13 digitos.',
        ];
    }
}
