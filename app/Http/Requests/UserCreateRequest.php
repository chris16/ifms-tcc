<?php

namespace Tcc\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required',
            'razao_social' => 'required|min:5',
            'fantasia' => 'required',
            'cnpj' => 'required|unique:empresas',
            'email' => 'required|email|max:150|unique:empresas',
            'telefone' => 'required',
            'endereco' => 'required',
            'bairro' => 'required',
            'numero' => 'required',
            'cep' => 'required',
            'nome_do_responsavel' => 'required',
            'email_do_responsavel' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }

    public function messages()
    {
        return [
            'razao_social.required' => 'Por favor, preencha o campo RAZÃO SOCIAL.',
            'razao_social.min' => 'Por favor, informe um RAZÃO SOCIAL de no mínimo :min caracteres.',
            'fantasia.required' => 'Por favor, preencha o campo FANTASIA.',
            'cnpj.required' => 'Por favor, preencha o campo CNPJ.',
            'cnpj.unique' => 'O CNPJ informado para a empresa já está cadastrado no sistema.',
            'email.required' => 'Por favor, preencha o campo E-MAIL.',
            'email.email' => 'Por favor, o formato de e-mail está inválido.',
            'email.unique' => 'O E-MAIL informado já está cadastrado no sistema.',
            'email.max' => 'Por favor, informe um E-MAIL de no máximo :max caracteres.',
            'endereco.required' => 'Por favor, preencha o campo CATEGORIA.',
            'telefone.required' => 'Por favor, preencha o campo DESCRIÇÃO.',
            'numero.required' => 'Por favor, preencha o campo PRAZO DE PRODUÇÃO.',
            'cep.required' => 'Por favor, ipreencha o campo CEP.',
            'bairro.required' => 'Por favor, preencha o campo BAIRRO.',
            'cidade_id.required' => 'Por favor, preencha o campo CIDADE.',
            'nome_do_responsavel.required' => 'Por favor, preencha o campo LARGURA.',
            'email_do_responsavel.required' => 'Por favor, preencha o campo E-MAIL.',
            'email_do_responsavel.email' => 'Por favor, o formato de e-mail está inválido.',
            'password.required' => 'Por favor, preencha o campo SENHA.',
            'password.min' => 'Por favor, informe uma SENHA de no mínimo :min caracteres.',
            'password.confirmed' => 'A confirmação da senha não corresponde.',
            ];
    }
}
