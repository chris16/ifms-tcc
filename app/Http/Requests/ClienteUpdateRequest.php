<?php

namespace Tcc\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:5',
            'email' => 'required|email|max:150|unique:clientes,id,'.$this->get('id'),
            'telefone' => 'required',
            'endereco' => 'required',
            'bairro' => 'required',
            'numero' => 'required',
            'cep' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Por favor, preencha o campo NOME.',
            'nome.min' => 'Por favor, informe um NOME de no mínimo :min caracteres.',
            'email.required' => 'Por favor, preencha o campo E-MAIL.',
            'email.email' => 'Por favor, o formato de e-mail está inválido.',
            'email.unique' => 'O E-MAIL informado já está cadastrado no sistema.',
            'email.max' => 'Por favor, informe um E-MAIL de no máximo :max caracteres.',
            'endereco.required' => 'Por favor, preencha o campo CATEGORIA.',
            'telefone.required' => 'Por favor, preencha o campo DESCRIÇÃO.',
            'numero.required' => 'Por favor, preencha o campo PRAZO DE PRODUÇÃO.',
            'cep.required' => 'Por favor, ipreencha o campo CEP.',
            'bairro.required' => 'Por favor, preencha o campo BAIRRO.',
        ];
    }
}
