<?php

namespace Tcc\Http\Controllers;

use Carbon\Carbon;
use Tcc\Http\Requests\CategoriaCreateRequest;
use Tcc\Models\Categoria;
use Tcc\Repositories\CategoriaRepository;

class CategoriasController extends Controller
{
    /** @var  CategoriaRepository */
    protected $repository;

    /**
     * CategoriasController constructor.
     * @param CategoriaRepository $repository
     */
    public function __construct(CategoriaRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $categorias = $this->repository->all();

        return view('categorias.show', compact('categorias'));
    }


    /**
     * @param CategoriaCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoriaCreateRequest $request)
    {
        $this->repository->create($request->all());
        flash()->success('Categoria cadastrada com sucesso.');

        return back();
    }


    /**
     * @param int $id
     * @param int $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus($id, $status)
    {
        $this->repository->update(['ativo' => $status], $id);
        flash()->success('Categoria alterada com sucesso.');

        return back();
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function softDelete($id)
    {
        $this->repository->delete($id);
        flash()->success('Categoria removida com sucesso.');

        return back();

    }
}
