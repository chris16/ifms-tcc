<?php

namespace Tcc\Http\Controllers;

use Tcc\Http\Requests\FormaPagamentoCreateRequest;
use Tcc\Repositories\EmpresaRepository;
use Tcc\Repositories\FormaPagamentoRepository;

class FormasPagamentoController extends Controller
{

    /** @var  FormaPagamentoRepository */
    protected $repository;


    /**
     * FormasPagamentoController constructor.
     * @param FormaPagamentoRepository $repository
     */
    public function __construct(FormaPagamentoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function show()
    {
        $formasDePagamento = $this->repository->all();

        return view('formasPagamentos.show', compact('formasDePagamento'));
    }


    /**
     * @param FormaPagamentoCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormaPagamentoCreateRequest $request)
    {
        $this->repository->create($request->all());
        flash()->success('Forma de Pagamento Cadastrada com sucesso.');

        return redirect()->action('FormasPagamentoController@show');
    }


    public function changeStatus($id, $status){
        $this->repository->update(['ativo' => $status],$id);
        flash()->success('Forma de Pagamento Alterada com sucesso.');

        return redirect()->action('FormasPagamentoController@show');
    }

}
