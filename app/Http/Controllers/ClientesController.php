<?php

namespace Tcc\Http\Controllers;


use Tcc\Http\Requests\ClienteCreateRequest;
use Tcc\Http\Requests\ClienteUpdateRequest;
use Tcc\Repositories\ClienteRepository;
use Tcc\Repositories\EmpresaRepository;

class ClientesController extends Controller
{
    /** @var  ClienteRepository */
    private $repository;

    /** @var  EmpresaRepository */
    private $empresaRepository;

    /**
     * ClientesController constructor.
     * @param ClienteRepository $repository
     * @param EmpresaRepository $empresaRepository
     */
    public function __construct(ClienteRepository $repository, EmpresaRepository $empresaRepository)
    {
        $this->repository = $repository;
        $this->empresaRepository = $empresaRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAll()
    {
        $clientes = $this->repository->paginate(10);

        return view('clientes.showAll', compact('clientes'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * @param ClienteCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ClienteCreateRequest $request)
    {
        $this->repository->create($request->all());
        flash()->success('Cliente Cadastrado Com Sucesso');

        return redirect()->back();
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id){
        $cliente = $this->repository->find($id);

        return view('clientes.edit',compact('cliente','empresas'));
    }

    /**
     * @param ClienteUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ClienteUpdateRequest $request,$id){
        $this->repository->update($request->all(),$id);

        flash()->success('Dados do Cliente Atualizados com Sucesso');

        return redirect()->action('ClientesController@showAll');
    }
}
