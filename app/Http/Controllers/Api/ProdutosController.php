<?php

namespace Tcc\Http\Controllers\Api;

use Illuminate\Http\Request;
use Tcc\Criteria\FiltroPorEmpresaCriteria;
use Tcc\Http\Controllers\Controller;
use Tcc\Repositories\ProdutoRepository;

class ProdutosController extends Controller
{
    /** @var  ProdutoRepository */
    protected $repository;

    /**
     * ProdutosController constructor.
     * @param ProdutoRepository $repository
     */
    public function __construct(ProdutoRepository $repository)
    {
        $this->repository = $repository;
    }


    public function index()
    {
        $this->repository->pushCriteria(FiltroPorEmpresaCriteria::class);
        $produtos = $this->repository->all();

        return response()->json($produtos);
    }


}
