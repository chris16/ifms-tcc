<?php

namespace Tcc\Http\Controllers;

use Illuminate\Http\Request;
use Tcc\Http\Requests\UserCreateRequest;
use Tcc\Http\Services\CadastrarUsuarioService;

class UsersController extends Controller
{
    /** @var  CadastrarUsuarioService */
    protected $usuarioService;

    /**
     * UsersController constructor.
     * @param CadastrarUsuarioService $usuarioService
     */
    public function __construct(CadastrarUsuarioService $usuarioService)
    {
        $this->usuarioService = $usuarioService;
    }

    /**
     * @param UserCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(UserCreateRequest $request){
        try {
            $this->usuarioService->save($request->all());
            flash()->success('Cadastro Realizado com sucesso');

            return redirect()->action('PedidosController@showAll');
        }catch (\Exception $e){
            echo $e->getMessage();
            exit;
        }
    }


}
