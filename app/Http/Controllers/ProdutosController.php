<?php

namespace Tcc\Http\Controllers;

use Illuminate\Http\Request;
use Prettus\Validator\Exceptions\ValidatorException;
use Tcc\Http\Requests\ProdutoCreateRequest;
use Tcc\Http\Requests\ProdutoUpdateRequest;
use Tcc\Repositories\CategoriaRepository;
use Tcc\Repositories\ProdutoRepository;

class ProdutosController extends Controller
{
    /** @var  ProdutoRepository */
    protected $repository;

    /** @var  CategoriaRepository */
    protected $categoriaRepository;

    /**
     * ProdutosController constructor.
     * @param ProdutoRepository $repository
     * @param CategoriaRepository $categoriaRepository
     */
    public function __construct(ProdutoRepository $repository, CategoriaRepository $categoriaRepository)
    {
        $this->repository = $repository;
        $this->categoriaRepository = $categoriaRepository;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categorias = $this->categoriaRepository->listarTodasCategoriasForm();

        return view('produtos.create', compact('categorias'));
    }


    /**
     * @param  ProdutoCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutoCreateRequest $request)
    {
        try {
            $request->request->add(['user_id' => auth()->user()->id]);
            $this->repository->create($request->all());
            flash()->success('Empresa Cadastrado Com Sucesso');

            return redirect()->back();

        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function showAll()
    {
        $produtos = $this->repository->with('categoria')->paginate(15);

        return view('produtos.showAll', compact('produtos'));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $produto = $this->repository->find($id);
        $categorias = $this->categoriaRepository->listarTodasCategoriasForm();

        return view('produtos.edit', compact('produto', 'categorias'));
    }

    /**
     * @param ProdutoUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProdutoUpdateRequest $request, $id)
    {
        try {
            $this->repository->update($request->all(), $id);

            flash()->success('Os dados do produto foram alterados com sucesso.');

            return back();
        } catch (ValidatorException $e) {
            flash()->error($e->getMessageBag());

            return back();
        }
    }

    /**
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Produto apagado.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Produto removida.');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function findProdutosPorCategoriaAjax(Request $request)
    {
        $produtos = [];
        if (isset($request->categoria) && !empty($request->categoria)) {
            $produtos = $this->repository->buscaProdutosPorCategoriaForm($request->categoria);
        }

        return response()->json($produtos);
    }


}
