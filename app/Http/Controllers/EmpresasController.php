<?php

namespace Tcc\Http\Controllers;

use Illuminate\Http\Request;

use Tcc\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Tcc\Http\Requests\EmpresaCreateRequest;
use Tcc\Http\Requests\EmpresaUpdateRequest;
use Tcc\Repositories\CidadeRepository;
use Tcc\Repositories\EmpresaRepository;
use Tcc\Validators\EmpresaValidator;


class EmpresasController extends Controller
{

    /**
     * @var EmpresaRepository
     */
    protected $repository;
    
    /**
     * EmpresasController constructor.
     * @param EmpresaRepository $repository
     */
    public function __construct(EmpresaRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('empresas.create');
    }


    /**
     * @param  EmpresaCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpresaCreateRequest $request)
    {
        try {
            $request->request->add(['user_id' => auth()->user()->id]);
            $this->repository->create($request->all());
            flash()->success('Empresa Cadastrado Com Sucesso');

            return redirect()->back();

        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function showAll()
    {
        $empresas = $this->repository->scopeQuery(function ($query) {
            return $query->where('user_id', auth()->user()->id);
        })->paginate(15);

        return view('empresas.showAll', compact('empresas'));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        $id = auth()->user()->empresa_id;
        $empresa = $this->repository->find($id);

         return view('empresas.edit', compact('empresa'));
    }

    /**
     * @param EmpresaUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EmpresaUpdateRequest $request)
    {
        try {
            $id = auth()->user()->empresa_id;
            $this->repository->update($request->all(), $id);

            flash()->success('Os dados da empresa foram alterados com sucesso.');

            return back();
        } catch (ValidatorException $e) {
            flash()->error($e->getMessageBag());

            return back();
        }
    }

    /**
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Empresa deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Empresa removida.');
    }
}
