<?php

namespace Tcc\Http\Controllers;

use Illuminate\Http\Request;
use Tcc\Http\Requests\PedidoCreateRequest;
use Tcc\Repositories\PedidoItemRepository;
use Tcc\Services\CarrinhoService;
use Tcc\Repositories\CategoriaRepository;
use Tcc\Repositories\ClienteRepository;
use Tcc\Repositories\FormaPagamentoRepository;
use Tcc\Repositories\PedidoRepository;
use Tcc\Repositories\ProdutoRepository;
use Gloudemans\Shoppingcart\Facades\Cart;

class PedidosController extends Controller
{
    /** @var  PedidoRepository */
    protected $repository;

    /** @var  CarrinhoService */
    protected $carrinhoService;

    /** @var  CategoriaRepository */
    protected $categoriaRepository;

    /** @var  ClienteRepository */
    protected $clienteRepository;

    /** @var  FormaPagamentoRepository */
    protected $formaPagamentoRepository;

    /** @var  PedidoItemRepository */
    protected $itemPedidoRepository;

    /** @var  ProdutoRepository */
    protected $produtoRepository;

    /**
     * PedidosController constructor.
     * @param PedidoRepository $repository
     * @param CategoriaRepository $categoriaRepository
     * @param ClienteRepository $clienteRepository
     * @param CarrinhoService $carrinhoService
     * @param FormaPagamentoRepository $formaPagamentoRepository
     * @param PedidoItemRepository $itemPedidoRepository
     * @param ProdutoRepository $produtoRepository
     */
    public function __construct(
        PedidoRepository $repository,
        CarrinhoService $carrinhoService,
        CategoriaRepository $categoriaRepository,
        ClienteRepository $clienteRepository,
        FormaPagamentoRepository $formaPagamentoRepository,
        PedidoItemRepository $itemPedidoRepository,
        ProdutoRepository $produtoRepository
    )
    {
        $this->repository = $repository;
        $this->categoriaRepository = $categoriaRepository;
        $this->clienteRepository = $clienteRepository;
        $this->formaPagamentoRepository = $formaPagamentoRepository;
        $this->carrinhoService = $carrinhoService;
        $this->itemPedidoRepository = $itemPedidoRepository;
        $this->produtoRepository = $produtoRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categorias = $this->categoriaRepository->listarTodasCategoriasForm();
        $clientes = $this->clienteRepository->listarTodosClientesForm();
        $formasPagamento = $this->formaPagamentoRepository->listarTodasFormasDePagamentoForm();

        return view('pedidos.create', compact('categorias', 'clientes', 'formasPagamento'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addToCart(Request $request)
    {
        try {
            $qtd = $request->quantidade;
            $produto = $this->produtoRepository->find($request->produto);
            $add = $this->carrinhoService->add($produto, $qtd);

            if ($add) {
                flash()->success('Produto adicionado ao pedido');
                return back();
            }

            flash()->warning('Desculpe, esse produto não pode ser acrescentado ao pedido');
            return back();

        } catch (\Exception $e) {
            flash()->error($e->getMessage());
        }
    }

    /**
     * @param PedidoCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PedidoCreateRequest $request)
    {
        if (Cart::content()->count() > 0) {
            try {
                $request->request->add([
                    'total' => str_replace(',', '', Cart::subtotal()),
                    'qtd_itens' => Cart::count()
                ]);

                $pedido = $this->repository->create($request->all());
                $this->saveItensPedido($pedido->id);

                $this->carrinhoService->clearCart();
                flash()->success('Pedido Realizado com sucesso.');

                return back();

            } catch (\Exception $e) {
                flash()->warning($e->getMessage());
                return back();
            }
        }

        flash()->warning('Seu carrinho está vazio');

        return back();
    }

    public function showAll()
    {
        $pedidos = $this->repository->paginate(15);

        return view('pedidos.showAll',compact('pedidos'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeItemPedido(Request $request)
    {
        $row = $request->produto;
        $this->carrinhoService->remove($row);
        flash()->success('Produto removido do Pedido');

        return back();
    }


    /**
     * @param $idPedido
     */
    private function saveItensPedido($idPedido)
    {
        foreach (Cart::content() as $item) {
            $this->itemPedidoRepository->create([
                'pedido_id' => $idPedido,
                'produto_id' => $item->id,
                'quantidade' => $item->qty,
                'valor_pago' => $item->price
            ]);
        }
    }


}
