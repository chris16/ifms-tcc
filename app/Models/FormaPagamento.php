<?php

namespace Tcc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Tcc\Scopes\TenantGlobalScope;
use Tcc\Scopes\TenantModelsScope;

class FormaPagamento extends Model implements Transformable
{
    use TransformableTrait,TenantModelsScope;

    protected $fillable = ['forma_pagamento','ativo','empresa_id'];

    public function empresa(){
        return $this->belongsToMany(FormaPagamento::class);
    }

}
