<?php

namespace Tcc\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Tcc\Scopes\TenantModelsScope;

class Categoria extends Model implements Transformable
{
    use TransformableTrait, TenantModelsScope, SoftDeletes;

    protected $fillable = ['categoria', 'empresa_id','ativo'];

    protected $dates = ['deleted_at'];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }


}
