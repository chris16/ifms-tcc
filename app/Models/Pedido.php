<?php

namespace Tcc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Tcc\Scopes\TenantGlobalScope;
use Tcc\Scopes\TenantModelsScope;

class Pedido extends Model implements Transformable
{
    use TransformableTrait,TenantModelsScope;

    protected $fillable = ['id', 'total', 'desconto', 'qtd_itens', 'pago', 'forma_pagamento_id', 'empresa_id', 'cliente_id'];

    public function formaPagamento()
    {
        return $this->belongsTo(FormaPagamento::class);
    }

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function itens(){
        return $this->belongsToMany(PedidoItem::class);
    }

    public function getTotalAttribute(){
        return numberUsaToBr($this->attributes['total']);
    }

}
