<?php

namespace Tcc\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Tcc\Scopes\TenantGlobalScope;
use Tcc\Scopes\TenantModelsScope;

class Produto extends Model implements Transformable
{
    use TransformableTrait, TenantModelsScope, SoftDeletes;

    protected $fillable = ['produto', 'descricao', 'categoria_id', 'empresa_id', 'preco', 'ativo'];


    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class)->withTrashed();
    }

    public function setPrecoAttribute($preco)
    {
        $this->attributes['preco'] = $preco;
    }

    public function getPrecoAttribute()
    {
        return $this->attributes['preco'];
    }

}
