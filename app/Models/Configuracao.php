<?php

namespace Tcc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Tcc\Scopes\TenantGlobalScope;

class Configuracao extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [];

}
