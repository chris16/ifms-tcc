<?php

namespace Tcc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Empresa extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['razao_social', 'logomarca', 'fantasia', 'cnpj', 'email', 'telefone', 'ramal', 
        'celular', 'cep', 'endereco', 'numero', 'complemento', 'bairro', 'nome_do_responsavel', 
        'email_do_responsavel', 'ativo'];

    public function formaPagamentos()
    {
        return $this->hasMany(FormaPagamento::class);
    }

}
