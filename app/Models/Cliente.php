<?php

namespace Tcc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Tcc\Scopes\TenantModelsScope;

class Cliente extends Model implements Transformable
{
    use TransformableTrait, TenantModelsScope;

    protected $fillable = ['nome', 'empresa_id', 'cep', 'endereco', 'numero', 'bairro', 'complemento', 'telefone',
        'email', 'empresa_id'];


    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }
    
}
