<?php

namespace Tcc\Scopes;

use Illuminate\Database\Eloquent\Model;

trait TenantModelsScope
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            if(auth()->check()) {
                $empresaId = auth()->user()->empresa_id;
                $model->empresa_id = $empresaId;
            }
        });
    }
}