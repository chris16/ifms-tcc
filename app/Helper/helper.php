<?php

if (!function_exists('numberBrToUsa')) {
    function numberBrToUsa($number)
    {
        return floatval(str_replace(',', '.', str_replace('.', '', $number)));
    }
}

if (!function_exists('numberUsaToBr')) {
    function numberUsaToBr($number)
    {
        return number_format($number, 2, ',', '.');
    }
}