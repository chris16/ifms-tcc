<?php

namespace Tcc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Tcc\Criteria\FiltroPorEmpresaCriteria;
use Tcc\Repositories\UserRepository;
use Tcc\Models\User;
use Tcc\Validators\UserValidator;

/**
 * Class UserRepositoryEloquent
 * @package namespace Tcc\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(FiltroPorEmpresaCriteria::class);
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
