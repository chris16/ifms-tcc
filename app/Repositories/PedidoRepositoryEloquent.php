<?php

namespace Tcc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Tcc\Criteria\FiltroPorEmpresaCriteria;
use Tcc\Repositories\PedidoRepository;
use Tcc\Models\Pedido;
use Tcc\Validators\PedidoValidator;

/**
 * Class PedidoRepositoryEloquent
 * @package namespace Tcc\Repositories;
 */
class PedidoRepositoryEloquent extends BaseRepository implements PedidoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Pedido::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(FiltroPorEmpresaCriteria::class);
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
