<?php

namespace Tcc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProdutoRepository
 * @package namespace Tcc\Repositories;
 */
interface ProdutoRepository extends RepositoryInterface
{
    //
}
