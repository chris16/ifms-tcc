<?php

namespace Tcc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ClienteRepository
 * @package namespace Tcc\Repositories;
 */
interface ClienteRepository extends RepositoryInterface
{
    //
}
