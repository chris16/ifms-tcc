<?php

namespace Tcc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsuariosNiveisRepository
 * @package namespace Tcc\Repositories;
 */
interface UsuariosNiveisRepository extends RepositoryInterface
{
    //
}
