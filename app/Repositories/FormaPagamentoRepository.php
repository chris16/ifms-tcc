<?php

namespace Tcc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FormaPagamentoRepository
 * @package namespace Tcc\Repositories;
 */
interface FormaPagamentoRepository extends RepositoryInterface
{
    //
}
