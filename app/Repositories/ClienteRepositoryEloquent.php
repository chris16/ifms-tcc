<?php

namespace Tcc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Tcc\Criteria\FiltroPorEmpresaCriteria;
use Tcc\Repositories\ClienteRepository;
use Tcc\Models\Cliente;
use Tcc\Validators\ClienteValidator;

/**
 * Class ClienteRepositoryEloquent
 * @package namespace Tcc\Repositories;
 */
class ClienteRepositoryEloquent extends BaseRepository implements ClienteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Cliente::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(FiltroPorEmpresaCriteria::class);
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return mixed
     */
    public function listarTodosClientesForm()
    {
        return $this->scopeQuery(function ($query) {
            return $query->orderBy('nome');
        })->all()->pluck('nome', 'id');
    }
}
