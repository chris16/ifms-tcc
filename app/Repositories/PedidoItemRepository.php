<?php

namespace Tcc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PedidoItemRepository
 * @package namespace Tcc\Repositories;
 */
interface PedidoItemRepository extends RepositoryInterface
{
    //
}
