<?php

namespace Tcc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Tcc\Repositories\UsuariosNiveisRepository;
use Tcc\Models\UsuariosNiveis;
use Tcc\Validators\UsuariosNiveisValidator;

/**
 * Class UsuariosNiveisRepositoryEloquent
 * @package namespace Tcc\Repositories;
 */
class UsuariosNiveisRepositoryEloquent extends BaseRepository implements UsuariosNiveisRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UsuariosNiveis::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
