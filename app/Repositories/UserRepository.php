<?php

namespace Tcc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace Tcc\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
