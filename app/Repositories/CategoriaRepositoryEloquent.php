<?php

namespace Tcc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Tcc\Criteria\FiltroPorEmpresaCriteria;
use Tcc\Repositories\CategoriaRepository;
use Tcc\Models\Categoria;
use Tcc\Validators\CategoriaValidator;

/**
 * Class CategoriaRepositoryEloquent
 * @package namespace Tcc\Repositories;
 */
class CategoriaRepositoryEloquent extends BaseRepository implements CategoriaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Categoria::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(FiltroPorEmpresaCriteria::class);
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return mixed
     */
    public function listarTodasCategoriasForm()
    {
        return $this->scopeQuery(function ($query) {
            return $query->orderBy('categoria');
        })->findWhere(['ativo' => '1'])->pluck('categoria', 'id');
    }
}
