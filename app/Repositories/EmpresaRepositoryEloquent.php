<?php

namespace Tcc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Tcc\Repositories\EmpresaRepository;
use Tcc\Models\Empresa;
use Tcc\Validators\EmpresaValidator;

/**
 * Class EmpresaRepositoryEloquent
 * @package namespace Tcc\Repositories;
 */
class EmpresaRepositoryEloquent extends BaseRepository implements EmpresaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Empresa::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
