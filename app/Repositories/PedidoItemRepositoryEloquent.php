<?php

namespace Tcc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Tcc\Repositories\PedidoItemRepository;
use Tcc\Models\PedidoItem;
use Tcc\Validators\PedidoItemValidator;

/**
 * Class PedidoItemRepositoryEloquent
 * @package namespace Tcc\Repositories;
 */
class PedidoItemRepositoryEloquent extends BaseRepository implements PedidoItemRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PedidoItem::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
