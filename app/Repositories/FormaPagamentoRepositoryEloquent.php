<?php

namespace Tcc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Tcc\Criteria\FiltroPorEmpresaCriteria;
use Tcc\Models\FormaPagamento;

/**
 * Class FormaPagamentoRepositoryEloquent
 * @package namespace Tcc\Repositories;
 */
class FormaPagamentoRepositoryEloquent extends BaseRepository implements FormaPagamentoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return FormaPagamento::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(FiltroPorEmpresaCriteria::class);
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * @return mixed
     */
    public function listarTodasFormasDePagamentoForm()
    {
        return $this->scopeQuery(function ($query) {
            return $query->orderBy('forma_pagamento');
        })->findByField('ativo','1')->pluck('forma_pagamento', 'id');
    }
}
