<?php

namespace Tcc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmpresaRepository
 * @package namespace Tcc\Repositories;
 */
interface EmpresaRepository extends RepositoryInterface
{
    //
}
