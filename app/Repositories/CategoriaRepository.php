<?php

namespace Tcc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoriaRepository
 * @package namespace Tcc\Repositories;
 */
interface CategoriaRepository extends RepositoryInterface
{
    //
}
