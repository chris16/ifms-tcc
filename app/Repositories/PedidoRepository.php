<?php

namespace Tcc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PedidoRepository
 * @package namespace Tcc\Repositories;
 */
interface PedidoRepository extends RepositoryInterface
{
   
}
