<?php

namespace Tcc\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Tcc\Repositories\UserRepository::class, \Tcc\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\Tcc\Repositories\EmpresaRepository::class, \Tcc\Repositories\EmpresaRepositoryEloquent::class);
        $this->app->bind(\Tcc\Repositories\UsuariosNiveisRepository::class, \Tcc\Repositories\UsuariosNiveisRepositoryEloquent::class);
        $this->app->bind(\Tcc\Repositories\ProdutoRepository::class, \Tcc\Repositories\ProdutoRepositoryEloquent::class);
        $this->app->bind(\Tcc\Repositories\FormaPagamentoRepository::class, \Tcc\Repositories\FormaPagamentoRepositoryEloquent::class);
        $this->app->bind(\Tcc\Repositories\PedidoRepository::class, \Tcc\Repositories\PedidoRepositoryEloquent::class);
        $this->app->bind(\Tcc\Repositories\ClienteRepository::class, \Tcc\Repositories\ClienteRepositoryEloquent::class);
        $this->app->bind(\Tcc\Repositories\CategoriaRepository::class, \Tcc\Repositories\CategoriaRepositoryEloquent::class);
        $this->app->bind(\Tcc\Repositories\PedidoItemRepository::class, \Tcc\Repositories\PedidoItemRepositoryEloquent::class);
        //:end-bindings:
    }
}
