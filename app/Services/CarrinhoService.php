<?php

namespace Tcc\Services;

use Gloudemans\Shoppingcart\Facades\Cart;
use Tcc\Models\Produto;

class CarrinhoService
{

    /**
     * @param Produto $produto
     * @param int $qtd
     * @return bool
     */
    public function add($produto,$qtd)
    {
        if (!empty($produto)) {
            Cart::add([
                'id' => $produto->id,
                'name' => $produto->produto,
                'qty' => $qtd,
                'price' => $produto->preco
            ]);

            return true;
        }

        return false;
    }

    /**
     * @param string $row
     * @param int $qtd
     */
    public function update($row,$qtd)
    {
        Cart::update($row, $qtd);
    }

    /**
     * @param string $row
     */
    public function remove($row)
    {
        Cart::remove($row);
    }


    public function clearCart()
    {
        Cart::destroy();
    }

}