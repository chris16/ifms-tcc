@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Cadastrar Nova Categoria</div>
                    <div class="panel-body">
                        @include('errors._check_form')
                        @include('flash::message')
                        {!! Form::open(['route' => 'sistema.categoria.store', 'method' => 'post' ]) !!}
                        @include('categorias._form')

                        <div class="form-group">
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-success">
                                    Cadastrar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Categorias</div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Categoria</th>
                                <th>Ativo</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($categorias as $categoria)
                                <tr>
                                    <td>{{$categoria->id}}</td>
                                    <td>{{$categoria->categoria}}</td>
                                    <td>{{($categoria->ativo)? 'Sim': 'Não'}}</td>
                                    <td class="text-right">
                                        @if($categoria->ativo == 0)
                                            <a href="{{route('sistema.categoria.change',[$categoria->id,1])}}"
                                               class="btn btn-success btn-sm">
                                                Ativar
                                            </a>
                                        @else
                                            <a href="{{route('sistema.categoria.change',[$categoria->id,0])}}"
                                               class="btn btn-warning btn-sm">
                                                Desativar
                                            </a>
                                        @endif
                                            <a href="{{route('sistema.categoria.delete',[$categoria->id])}}"
                                               class="btn btn-danger btn-sm">
                                                Remover
                                            </a>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="4">Nenhum registro encontrado.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
