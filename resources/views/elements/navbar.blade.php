<div class="collapse navbar-collapse" id="app-navbar-collapse">
    <ul class="nav navbar-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Clientes <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{route('sistema.clientes.create')}}">Novo Cliente</a></li>
                <li><a href="{{route('sistema.clientes.showAll')}}">Listar Todos</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Produtos <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{route('sistema.produto.create')}}">Novo Produto</a></li>
                <li><a href="{{route('sistema.produto.showAll')}}">Listar Todos</a></li>
                <li><a href="{{route('sistema.categoria.show')}}">Categorias</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pedidos <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{route('sistema.pedido.create')}}">Novo Pedido</a></li>
                <li><a href="{{route('sistema.pedido.showAll')}}">Listar Todos</a></li>
                <li><a href="{{route('sistema.formas.show')}}">Formas de Pagamento</a></li>
            </ul>
        </li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
        @if (Auth::guest())
            <li><a href="{{ url('/login') }}">Login</a></li>
            <li><a href="{{ url('/register') }}">Registrar</a></li>
        @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->login }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{route('sistema.empresas.edit')}}">Minha Empresa</a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Sair
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        @endif
    </ul>
</div>