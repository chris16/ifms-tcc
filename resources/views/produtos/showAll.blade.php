@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Produtos Cadastrados</div>
                    <div class="panel-body">
                        @include('errors._check_form')
                        @include('flash::message')
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Produto</th>
                                <th>Categoria</th>
                                <th>Preço</th>
                                <th>Ativo</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($produtos as $produto)
                                <tr>
                                    <td>{{$produto->id}}</td>
                                    <td>{{$produto->produto}}</td>
                                    <td>{{$produto->categoria->categoria}}</td>
                                    <td>R$ {{$produto->preco}}</td>
                                    <td>{{($produto->ativo)? 'sim':'não'}}</td>
                                    <td>
                                        <a href="{{route('sistema.produto.edit',[$produto->id])}}"
                                           class="btn btn-primary btn-sm">
                                            Editar
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $produtos->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection