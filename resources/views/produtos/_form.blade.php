<div class="col-md-6 form-group{{ $errors->has('produto') ? 'has-error' : '' }}">
    <label for="name" class="">Produto</label>
    {!! Form::text('produto',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('produto'))
        <span class="help-block">
          <strong>{{ $errors->first('produto') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-2 form-group{{ $errors->has('preco') ? 'has-error' : '' }}">
    <label for="name" class="">Preço</label>
    {!! Form::text('preco',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('preco'))
        <span class="help-block">
            <strong>{{ $errors->first('preco') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-4 form-group{{ $errors->has('categoria_id') ? 'has-error' : '' }}">
    <label for="name" class="">Categoria</label>
    {!! Form::select('categoria_id',[null => 'SELECIONE UMA CATEGORIA']+$categorias->toArray(),null,
       ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('categoria_id'))
        <span class="help-block">
            <strong>{{ $errors->first('categoria_id') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-12 form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
    <label for="name" class="">Descrição</label>
    {!! Form::textarea('descricao',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('descricao'))
        <span class="help-block">
            <strong>{{ $errors->first('descricao') }}</strong>
         </span>
    @endif
</div>

<div class="col-md-6 form-group{{ $errors->has('ativo') ? ' has-error' : '' }}">
    <label>Ativar?</label>
    <label class="radio-inline">
        {!! Form::radio('ativo', '1', true) !!} Sim
    </label>
    <label class="radio-inline">
        {!! Form::radio('ativo', '0', false) !!} Não
    </label>
    @if ($errors->has('ativo'))
        <span class="help-block">
            <strong>{{ $errors->first('ativo') }}</strong>
        </span>
    @endif
</div>