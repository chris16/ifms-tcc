@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Edição de Cadastro de Empresa</div>
                    <div class="panel-body">
                        @include('errors._check_form')
                        @include('flash::message')

                        {!! Form::model($produto,['route' => ['sistema.produto.update',$produto->id], 'method' => 'put']) !!}

                        @include('produtos._form')

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Salvar Alterações
                                </button>
                                <a href="{{route('sistema.produto.showAll')}}" class="btn btn-info">
                                    Voltar
                                </a>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
