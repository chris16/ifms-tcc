@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Edição de Cadastro de Cliente</div>
                    <div class="panel-body">
                        @include('errors._check_form')
                        @include('flash::message')

                        {!! Form::model($cliente,['route' => ['sistema.clientes.update',$cliente->id], 'method' => 'put']) !!}

                        @include('clientes._form')

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Salvar Alterações
                                </button>
                                <button type="reset" class="btn btn-warning">
                                    Limpar Cadastro
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
