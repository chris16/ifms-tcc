<div class="col-md-6 form-group{{ $errors->has('razao_social') ? ' has-error' : '' }}">
    <label for="name" class="">Nome</label>
    {!! Form::text('nome',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('razao_social'))
        <span class="help-block">
                                        <strong>{{ $errors->first('razao_social') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="name" class="">E-mail</label>
    {!! Form::email('email',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('email'))
        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-4 form-group{{ $errors->has('telefone') ? ' has-error' : '' }}">
    <label for="name" class="">Telefone</label>
    {!! Form::text('telefone',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('telefone'))
        <span class="help-block">
                                        <strong>{{ $errors->first('telefone') }}</strong>
                                    </span>
    @endif
</div>


<div class="col-md-3 form-group{{ $errors->has('cep') ? ' has-error' : '' }}">
    <label for="name" class="">CEP</label>
    {!! Form::text('cep',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('cep'))
        <span class="help-block">
                                        <strong>{{ $errors->first('cep') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-5 form-group{{ $errors->has('endereco') ? ' has-error' : '' }}">
    <label for="name" class="">Endereço</label>
    {!! Form::text('endereco',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('endereco'))
        <span class="help-block">
                                        <strong>{{ $errors->first('endereco') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-2 form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
    <label for="name" class="">Número</label>
    {!! Form::text('numero',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('numero'))
        <span class="help-block">
            <strong>{{ $errors->first('numero') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-3 form-group{{ $errors->has('bairro') ? ' has-error' : '' }}">
    <label for="name" class="">Bairro</label>
    {!! Form::text('bairro',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('bairro'))
        <span class="help-block">
            <strong>{{ $errors->first('bairro') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-7 form-group{{ $errors->has('complemento') ? ' has-error' : '' }}">
    <label for="name" class="">Complemento</label>
    {!! Form::text('complemento',null,['class' => 'form-control']) !!}
    @if ($errors->has('complemento'))
        <span class="help-block">
            <strong>{{ $errors->first('complemento') }}</strong>
        </span>
    @endif
</div>