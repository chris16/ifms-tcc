@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Clientes Cadastrados</div>
                    <div class="panel-body">
                        @include('errors._check_form')
                        @include('flash::message')
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>email</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clientes as $cliente)
                                <tr>
                                    <td>{{$cliente->id}}</td>
                                    <td>{{$cliente->nome}}</td>
                                    <td>{{$cliente->telefone}}</td>
                                    <td>{{$cliente->email}}</td>
                                    <td>
                                        <a href="{{route('sistema.clientes.edit',[$cliente->id])}}" class="btn btn-primary btn-sm">
                                            Editar
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $clientes->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection