@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Formulário de Cadastro de Empresa</div>
                    <div class="panel-body">
                        @include('errors._check_form')
                        @include('flash::message')
                        {!! Form::open(['route' => 'sistema.clientes.store', 'method' => 'post' ]) !!}

                        @include('clientes._form')

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Cadastrar
                                </button>
                                <button type="reset" class="btn btn-warning">
                                    Limpar Cadastro
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
