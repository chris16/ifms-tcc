@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Nova Forma de Pagamento</div>
                    <div class="panel-body">
                        @include('errors._check_form')
                        @include('flash::message')
                        {!! Form::open(['route' => 'sistema.formas.store', 'method' => 'post' ]) !!}

                        @include('formasPagamentos._form')

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Cadastrar
                                </button>
                                <button type="reset" class="btn btn-warning">
                                    Limpar Cadastro
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Formas de Pagamento Cadastradas</div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Forma de Pagamento</th>
                                <th>Ativo</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($formasDePagamento as $forma)
                                <tr>
                                    <td>{{$forma->id}}</td>
                                    <td>{{$forma->forma_pagamento}}</td>
                                    <td>{{($forma->ativo)? 'Sim': 'Não'}}</td>
                                    <td>
                                        @if($forma->ativo == 0)
                                            <a href="{{route('sistema.formas.change',[$forma->id,1])}}"
                                               class="btn btn-success btn-sm">
                                                Ativar
                                            </a>
                                        @else
                                            <a href="{{route('sistema.formas.change',[$forma->id,0])}}"
                                               class="btn btn-warning btn-sm">
                                                Desativar
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="4">Nenhum registro encontrado.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
