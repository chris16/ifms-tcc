<div class="col-md-8 form-group{{ $errors->has('forma_pagamento') ? ' has-error' : '' }}">
    <label for="name" class="">Forma de Pagamento</label>
    {!! Form::text('forma_pagamento',null,['class' => 'form-control','required']) !!}
</div>