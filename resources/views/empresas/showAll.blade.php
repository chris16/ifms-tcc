@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Empresas Cadastradas</div>
                    <div class="panel-body">
                        @include('errors._check_form')
                        @include('flash::message')
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Empresa</th>
                                <th>CNPJ</th>
                                <th>Telefone</th>
                                <th>Responsável</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($empresas as $empresa)
                                <tr>
                                    <td>{{$empresa->id}}</td>
                                    <td>{{$empresa->razao_social}}</td>
                                    <td>{{$empresa->cnpj}}</td>
                                    <td>{{$empresa->telefone}}</td>
                                    <td>{{$empresa->nome_do_responsavel}}</td>
                                    <td>
                                        <a href="{{route('sistema.empresas.edit',[$empresa->id])}}" class="btn btn-primary btn-sm">
                                            Editar
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $empresas->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection