<div class="col-md-6 form-group{{ $errors->has('razao_social') ? ' has-error' : '' }}">
    <label for="name" class="">Razão Social</label>
    {!! Form::text('razao_social',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('razao_social'))
        <span class="help-block">
                                        <strong>{{ $errors->first('razao_social') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-6 form-group{{ $errors->has('fantasia') ? ' has-error' : '' }}">
    <label for="name" class="">Nome Fantasia</label>
    {!! Form::text('fantasia',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('fantasia'))
        <span class="help-block">
                                        <strong>{{ $errors->first('fantasia') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-6 form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
    <label for="name" class="">CNPJ</label>
    {!! Form::text('cnpj',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('cnpj'))
        <span class="help-block">
                                        <strong>{{ $errors->first('cnpj') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="name" class="">E-mail</label>
    {!! Form::email('email',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('email'))
        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-4 form-group{{ $errors->has('telefone') ? ' has-error' : '' }}">
    <label for="name" class="">Telefone</label>
    {!! Form::text('telefone',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('telefone'))
        <span class="help-block">
                                        <strong>{{ $errors->first('telefone') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-2 form-group{{ $errors->has('ramal') ? ' has-error' : '' }}">
    <label for="name" class="">Ramal</label>
    {!! Form::text('ramal',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('ramal'))
        <span class="help-block">
                                        <strong>{{ $errors->first('ramal') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-3 form-group{{ $errors->has('celular') ? ' has-error' : '' }}">
    <label for="name" class="">Celular</label>
    {!! Form::text('celular',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('celular'))
        <span class="help-block">
                                        <strong>{{ $errors->first('celular') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-3 form-group{{ $errors->has('cep') ? ' has-error' : '' }}">
    <label for="name" class="">CEP</label>
    {!! Form::text('cep',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('cep'))
        <span class="help-block">
                                        <strong>{{ $errors->first('cep') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-6 form-group{{ $errors->has('endereco') ? ' has-error' : '' }}">
    <label for="name" class="">Endereço</label>
    {!! Form::text('endereco',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('endereco'))
        <span class="help-block">
                                        <strong>{{ $errors->first('endereco') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-2 form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
    <label for="name" class="">Número</label>
    {!! Form::text('numero',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('numero'))
        <span class="help-block">
            <strong>{{ $errors->first('numero') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-4 form-group{{ $errors->has('bairro') ? ' has-error' : '' }}">
    <label for="name" class="">Bairro</label>
    {!! Form::text('bairro',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('bairro'))
        <span class="help-block">
            <strong>{{ $errors->first('bairro') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-4 form-group{{ $errors->has('complemento') ? ' has-error' : '' }}">
    <label for="name" class="">Complemento</label>
    {!! Form::text('complemento',null,['class' => 'form-control']) !!}
    @if ($errors->has('complemento'))
        <span class="help-block">
            <strong>{{ $errors->first('complemento') }}</strong>
        </span>
    @endif
</div>


<div class="col-md-4 form-group{{ $errors->has('nome_do_responsavel') ? ' has-error' : '' }}">
    <label for="name" class="">Nome do Responsável</label>
    {!! Form::text('nome_do_responsavel',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('nome_do_responsavel'))
        <span class="help-block">
            <strong>{{ $errors->first('nome_do_responsavel') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-4 form-group{{ $errors->has('email_do_responsavel') ? ' has-error' : '' }}">
    <label for="name" class="">E-mail do Responsável</label>
    {!! Form::email('email_do_responsavel',null,['class' => 'form-control','required']) !!}
    @if ($errors->has('email_do_responsavel'))
        <span class="help-block">
            <strong>{{ $errors->first('email_do_responsavel') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-6 form-group{{ $errors->has('ativo') ? ' has-error' : '' }}">
    <label>Ativar?</label>
    <label class="radio-inline">
        {!! Form::radio('ativo', '1', true) !!} Sim
    </label>
    <label class="radio-inline">
        {!! Form::radio('ativo', '0', false) !!} Não
    </label>
    @if ($errors->has('ativo'))
        <span class="help-block">
            <strong>{{ $errors->first('ativo') }}</strong>
        </span>
    @endif
</div>