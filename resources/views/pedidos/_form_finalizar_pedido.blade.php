{!! Form::open(['route' => 'sistema.pedido.store', 'method' => 'post' ]) !!}

<div class="col-md-4 form-group{{ $errors->has('cliente_id') ? 'has-error' : '' }}">
    <label for="name" class="">Clientes</label>
    {!! Form::select('cliente_id',[null => 'SELECIONE UM CLIENTE']+$clientes->toArray(),null,
       ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('cliente_id'))
        <span class="help-block">
                                        <strong>{{ $errors->first('cliente_id') }}</strong>
                                    </span>
    @endif
</div>

<div class="col-md-4 form-group{{ $errors->has('forma_pagamento_id') ? 'has-error' : '' }}">
    <label for="name" class="">Forma De Pagamento</label>
    {!! Form::select('forma_pagamento_id',[null => 'SELECIONE UMA FORMA DE PAGAMENTO']
    +$formasPagamento->toArray(),
    null,['class' => 'form-control', 'required']) !!}
    @if ($errors->has('forma_pagamento_id'))
        <span class="help-block">
                                    <strong>{{ $errors->first('forma_pagamento_id') }}</strong>
                                </span>
    @endif
</div>

<div class="col-md-4 form-group" style="margin-top: 27px;">
    <button type="submit" class="btn btn-success">
        Finalizar Pedido
    </button>
</div>

<div class="form-group">
    <div class="col-md-4 col-md-offset-4">

    </div>
</div>

{!! Form::close() !!}