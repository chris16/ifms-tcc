{!! Form::open(['route' => 'sistema.pedido.addItem', 'method' => 'post' ]) !!}
<div class="col-md-4 form-group{{ $errors->has('categoria') ? 'has-error' : '' }}">
    <label for="name" class="">Categoria</label>
    {!! Form::select('categoria',[null => 'SELECIONE UMA CATEGORIA']+$categorias->toArray(),null,
       ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('categoria'))
        <span class="help-block">
            <strong>{{ $errors->first('categoria_id') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-4 form-group{{ $errors->has('produto') ? 'has-error' : '' }}">
    <label for="name" class="">Produto</label>
    {!! Form::select('produto',[null => 'SELECIONE UM PRODUTO'],null,
       ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('produto'))
        <span class="help-block">
            <strong>{{ $errors->first('produto') }}</strong>
        </span>
    @endif
</div>

<div class="col-md-2 form-group{{ $errors->has('quantidade') ? 'has-error' : '' }}">
    <label for="name" class="">Quantidade</label>
    {!! Form::number('quantidade',1,['class' => 'form-control', 'required','min' => 1]) !!}
    @if ($errors->has('quantidade'))
        <span class="help-block">
          <strong>{{ $errors->first('quantidade') }}</strong>
        </span>
    @endif
</div>
<div class="col-md-2 form-group" style="margin-top: 28px;">
    <button type="submit" class="btn btn-primary">
        Inserir Item
    </button>
</div>

{!! Form::close() !!}