@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Formulário de Pedido</div>
                    <div class="panel-body">
                        @include('errors._check_form')
                        @include('flash::message')

                        @include('pedidos._form')
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <th>#</th>
                            <th>Produto</th>
                            <th>Quantidade</th>
                            <th>Preço</th>
                            <th></th>
                            </thead>
                            <tbody>
                            @forelse(Cart::content() as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->qty}}</td>
                                    <td>R$ {{numberUsaToBr($item->price)}}</td>
                                    <td>
                                        {!! Form::open(['route' => 'sistema.carrinho.remove', 'method' => 'post']) !!}
                                        <input type="hidden" name="produto" value="{{$item->rowId}}">
                                        <button class="btn btn-danger">
                                            Remover
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="5"><b>Nenhum item adicionado</b></td>
                                </tr>
                            @endforelse
                            </tbody>
                            <tfooter>
                                <tr>
                                    <td class="text-left" colspan="4"><b>TOTAL</b></td>
                                    <td><b>R$ {{Cart::subtotal(2,',','.')}}</b></td>
                                </tr>
                            </tfooter>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if(Cart::count() > 0)
            <div class="row">
                <div class="col-md-12 ">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            @include('pedidos._form_finalizar_pedido')
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop

@section('scripts')
    <script>
        $('select[name=categoria]').on('change', function () {
            var categoria = $(this).val();
            $.ajax({
                url: '/sistema/ajax-busca-produto-por-categoria',
                method: 'post',
                data: {categoria: categoria},
                success: function (result) {
                    var option = '<option readonly="">SELECIONE UM PRODUTO</option>';
                    $.each(result, function (id, produto) {
                        option += '<option value="' + id + '">' + produto + '</option>';
                    });

                    $('select[name=produto]').html(option);
                }
            });
        });
    </script>
@stop
