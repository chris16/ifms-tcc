@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Pedidos Realizados</div>
                    <div class="panel-body">
                        @include('errors._check_form')
                        @include('flash::message')
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente</th>
                                <th>Qtd. Itens</th>
                                <th>Total</th>
                                <th>Data</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pedidos as $pedido)
                                <tr>
                                    <td>{{$pedido->id}}</td>
                                    <td>{{$pedido->cliente->nome}}</td>
                                    <td>{{$pedido->qtd_itens}}</td>
                                    <td>R$ {{$pedido->total}}</td>
                                    <td>{{$pedido->created_at->format('d/m/Y H:i:s')}}</td>
                                    <td></td>
                                    {{--<td>
                                        <a href="{{route('sistema.produto.edit',[$pedido->id])}}"
                                           class="btn btn-primary btn-sm">
                                            Editar
                                        </a>
                                    </td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $pedidos->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection